#!/usr/bin/python -u
# -*- coding: utf-8 -*-
#
#  nominatim_mp.py
#  
#  Copyright 2013 Christoph Fink <christoph@localhost.localdomain>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

from __future__ import print_function
import sys,os,time,multiprocessing,urllib2,json,signal,os.path
from datetime import datetime,timedelta
from subprocess import check_output
from ctypes import c_bool
#from collections import OrderedDict

# CONST/INPUT
csv_in="/mnt/super-fast-disk/bartosz/tw_all_lat_lon.csv"
csv_out="/mnt/large-space/bartosz/tw_output.csv"
csv_err="/mnt/large-space/bartosz/tw_errors.csv"
csv_offset="/mnt/large-space/bartosz/tw_offset"
nominatim_url='http://127.0.0.1/nominatim/reverse.php?lat=%0.10f&lon=%0.10f'
report_every=10000

all_labels={
	"loc_id": "",
	"place_id": "",
	#"licence": "",
	"osm_type": "",
	"osm_id": "",
	"lat": "",
	"lon": "",
	"category": "",
	"type": "",
	"addresstype": "",
	"display_name": "",
	"name": "",
	"continent": "", 
	"country": "", 
	"state": "", 
	"state district": "", 
	"county": "", 
	"city": "", 
	"city district": "", 
	"suburb": "", 
	"neighbourhood": "", 
	"region": "", 
	"island": "", 
	"administrative": "", 
	"postcode": "", 
	"town": "", 
	"village": "", 
	"hamlet": "", 
	"suburb": "", 
	"locality": "", 
	#"farm": "", 
	#"road": "", 
	#"bridleway": "", 
	#"cycleway": "", 
	#"pedestrian": "", 
	#"footway": "", 
	#"road": "", 
	#"industrial": "", 
	#"residential": "", 
	#"retail": "", 
	#"commercial": "", 
	#"airport": "", 
	#"station": "", 
	#"place of worship": "", 
	#"pub": "", 
	#"bar": "", 
	#"university": "", 
	#"museum": "", 
	#"arts centre": "", 
	#"zoo": "", 
	#"theme park": "", 
	#"attraction": "", 
	#"golf course": "", 
	#"castle": "", 
	#"hospital": "", 
	#"school": "", 
	#"theatre": "", 
	#"public building": "", 
	#"library": "", 
	#"townhall": "", 
	#"community centre": "", 
	#"fire station": "", 
	#"police": "", 
	#"bank": "", 
	#"post office": "", 
	#"park": "", 
	#"recreation ground": "", 
	#"hotel": "", 
	#"motel": "", 
	#"cinema": "", 
	#"information": "", 
	#"artwork": "", 
	#"archaeological site": "", 
	#"doctors": "", 
	#"sports centre": "", 
	#"swimming pool": "", 
	#"supermarket": "", 
	#"convenience": "", 
	#"restaurant": "", 
	#"fast food": "", 
	#"cafe": "", 
	#"guest house": "", 
	#"pharmacy": "", 
	#"fuel": "", 
	#"peak": "", 
	#"waterfall": "", 
	#"wood": "", 
	#"water": "", 
	#"forest": "", 
	#"cemetery": "", 
	#"allotments": "", 
	#"farmyard": "", 
	#"rail": "", 
	#"canal": "", 
	#"river": "", 
	#"stream": "", 
	#"bicycle": "", 
	#"clothes": "", 
	#"hairdresser": "", 
	#"doityourself": "", 
	#"estate agent": "", 
	#"car": "", 
	#"garden centre": "", 
	#"car repair": "", 
	#"newsagent": "", 
	#"bakery": "", 
	#"furniture": "", 
	#"butcher": "", 
	#"apparel": "", 
	#"electronics": "", 
	#"department store": "", 
	#"books": "", 
	#"yes": "", 
	#"outdoor": "", 
	#"mall": "", 
	#"florist": "", 
	#"charity": "", 
	#"hardware": "", 
	#"laundry": "", 
	#"shoes": "", 
	#"beverages": "", 
	#"dry cleaning": "", 
	#"carpet": "", 
	#"computer": "", 
	#"alcohol": "", 
	#"optician": "", 
	#"chemist": "", 
	#"gallery": "", 
	#"mobile phone": "", 
	#"sports": "", 
	#"jewelry": "", 
	#"pet": "", 
	#"beauty": "", 
	#"stationery": "", 
	#"shopping centre": "", 
	#"general": "", 
	#"electrical": "", 
	#"toys": "", 
	#"jeweller": "", 
	#"betting": "", 
	#"household": "", 
	#"travel agency": "", 
	#"hifi": "", 
	#"shop": "", 
	#"house": "", 
	#"house number": "", 
	#"country code": "", 
	#"pitch": "", 
	#"unsurfaced": "", 
	#"ruins": "", 
	#"college": "", 
	#"monument": "", 
	#"subway": "", 
	#"memorial": "", 
	#"nature reserve": "", 
	#"common": "", 
	#"lock gate": "", 
	#"fell": "", 
	#"nightclub": "", 
	#"path": "", 
	#"garden": "", 
	#"reservoir": "", 
	#"playground": "", 
	#"stadium": "", 
	#"mine": "", 
	#"cliff": "", 
	#"caravan site": "", 
	#"bus station": "", 
	#"kindergarten": "", 
	#"construction": "", 
	#"atm": "", 
	#"emergency phone": "", 
	#"lock": "", 
	#"riverbank": "", 
	#"coastline": "", 
	#"viewpoint": "", 
	#"hostel": "", 
	#"bed and breakfast": "", 
	#"halt": "", 
	#"platform": "", 
	#"tram": "", 
	#"courthouse": "", 
	#"recycling": "", 
	#"dentist": "", 
	#"beach": "", 
	#"moor": "", 
	#"grave yard": "", 
	#"derelict canal": "", 
	#"drain": "", 
	#"grass": "", 
	#"village green": "", 
	#"bay": "", 
	#"tram stop": "", 
	#"marina": "", 
	#"stile": "", 
	#"moor": "", 
	#"light rail": "", 
	#"narrow gauge": "", 
	#"land": "", 
	#"village hall": "", 
	#"dock": "", 
	#"veterinary": "", 
	#"brownfield": "", 
	#"track": "", 
	#"historic station": "", 
	#"construction": "", 
	#"prison": "", 
	#"quarry": "", 
	#"telephone": "", 
	#"traffic signals": "", 
	#"heath": "", 
	#"house": "", 
	#"social club": "", 
	#"military": "", 
	#"health centre": "", 
	#"building": "", 
	#"clinic": "", 
	#"services": "", 
	#"ferry terminal": "", 
	#"marsh": "", 
	#"hill": "", 
	#"raceway": "", 
	#"taxi": "", 
	#"take away": "", 
	#"car rental": "", 
	#"islet": "", 
	#"nursery": "", 
	#"nursing home": "", 
	#"toilets": "", 
	#"hall": "", 
	#"boatyard": "", 
	#"mini roundabout": "", 
	#"manor": "", 
	#"chalet": "", 
	#"bicycle parking": "", 
	#"hotel": "", 
	#"weir": "", 
	#"wetland": "", 
	#"cave entrance": "", 
	#"crematorium": "", 
	#"picnic site": "", 
	#"wood": "", 
	#"basin": "", 
	#"tree": "", 
	#"slipway": "", 
	#"meadow": "", 
	#"piste": "", 
	#"care home": "", 
	#"club": "", 
	#"medical centre": "", 
	#"roman road": "", 
	#"fort": "", 
	#"subway entrance": "", 
	#"yes": "", 
	#"gate": "", 
	#"fishing": "", 
	#"museum": "", 
	#"car wash": "", 
	#"level crossing": "", 
	#"bird hide": "", 
	#"headland": "", 
	#"apartments": "", 
	#"shopping": "", 
	#"scrub": "", 
	#"fen": "", 
	#"building": "", 
	#"mountain pass": "", 
	#"parking": "", 
	#"bus stop": "", 
	#"postcode": "", 
	#"post box": "", 
	#"houses": "", 
	#"preserved": "", 
	#"derelict canal": "", 
	#"dead pub": "", 
	#"disused station": "", 
	#"abandoned": "", 
	#"disused": "" 
}
order_of_columns=all_labels.keys() # if we convert this to a list here, we can refer to it later (without changing the order – this should (in theory) also be stable over several runs on the same system)

def main():
	
	try:
		input_queue=multiprocessing.Queue(1024) # FIFO-stack
		output_queue=multiprocessing.Queue(10240)
		stop=multiprocessing.Value(c_bool)
		stop.value=False
		threads=[]
	
		# one thread to process input
		p=multiprocessing.Process(target=parseinput,args=(input_queue,csv_in,csv_out,csv_offset,stop,order_of_columns))
		p.start()
		threads.append(p)
		
		# multiple threads to do the calc 
		for i in range(multiprocessing.cpu_count()+1):
			p=multiprocessing.Process(target=worker,args=(input_queue,output_queue,nominatim_url,all_labels,order_of_columns))
			p.start()
			threads.append(p)
	
		# one thread to collect output and write it to output file
		p=multiprocessing.Process(target=collectoutput,args=(output_queue,csv_in,csv_out,csv_err,csv_offset,report_every))
		p.start()
		threads.append(p)
		
		for t in threads:
			t.join()
	except KeyboardInterrupt:
		print("Sending stop request to child processes")
		stop.value=True
		signal.signal(signal.SIGINT, signal.SIG_IGN) # ignore further CTRL-Cs
	except Exception, e:
		print("Some error occurred, emergency stop initiated.")
		print(str(e))
		try:
			for t in threads:
				t.terminate()
		except:
			pass
		sys.exit()




def parseinput(input_queue,csv_in,csv_out,csv_offset,stop,order_of_columns):
	signal.signal(signal.SIGINT, signal.SIG_IGN)
	count=0
	offset=-1
	try:
		try:
			offset_=open(csv_offset,"r")
			offset=int(offset_.readline())
			offset_.close()
			print("Starting processing with an offset of %d lines" %(offset,))
		except:
			offset=-1
			print("No previously saved offset found, starting at line 0")
			csv_out=open(csv_out, "w")
			csv_out.write(";".join(order_of_columns)+"\n")
			csv_out.close()
			print("Truncating output file")
		try:
			csv=open(csv_in,"r")
		except:
			print("Could not open input file from %s" %(csv_in))
			exit()
		line=csv.readline()
		while line:
			if count > offset:
				input_queue.put((line,))
			line=csv.readline()
			count+=1
			if stop.value:
				# remember current offset in file:
				try:
					offset_=open(csv_offset,"w")
					offset_.write("%d\n" %(count,))
					offset_.close()
					print("Stop request received, saving offset: %d lines" %(count,))
				except:
					print("Stop request received, could not save offset (%d lines) to offset file (%s)" %(count,csv_offset))
				raise Exception("Stopping")
	except Exception,e:
		print(str(e))
	finally:  # if all lines put into queue:
		try:
			csv.close()
		except:
			pass
		for i in range(multiprocessing.cpu_count()+1):
			input_queue.put(("DEATH_PILL_STOP_NOW",))
		
def worker(input_queue,output_queue,nominatim_url,all_labels,order_of_columns):
	signal.signal(signal.SIGINT, signal.SIG_IGN) # ignore interrupt signals -> handle ctrl-c in main process
	try:
		while True:
				(line,)=input_queue.get()
				if line=='DEATH_PILL_STOP_NOW': 
					break # really break out of the loop, raising a double exception is more cumbersome than that
				try:
					(lat,lon,loc_id)=line.split(";")
					lat=float(lat)
					lon=float(lon)
					loc_id=int(loc_id)
				except:	
					print("could not read line: %s" %(repr(line)))
					continue 
				try:
					nominatim=urllib2.urlopen(nominatim_url %(lat,lon))
				except Exception,e : 
					print("failed to fetch nominatim information for") # %d @ %0.2f,0.2f" %(loc_id,lat,lon))
					print(loc_id,lat,lon)
					print(str(e))
				try:
					nominatim=json.loads(nominatim.read())
				except:
					print(nominatim)
				is_error=True if "error" in nominatim else False  # {"error": "Unable to geocode"}
				
				tmp=all_labels
				tmp["loc_id"]="%d" %(loc_id)
				for (k,v) in nominatim.iteritems():
					if type(v)==dict:
						for (kk,vv) in v.iteritems():
							tmp[kk]='"'+unicode(vv).replace('"','\"')+'"'
					else:
						tmp[k]='"'+unicode(v).replace('"','\"')+'"'
				
				nominatim=[]
				for o in order_of_columns:
					nominatim.append(tmp[o])
				
				nominatim=u';'.join(nominatim)+"\n"
				tmp=None
			
				output_queue.put((nominatim.encode('utf-8'),is_error))

#				line=None
#				lat=None
#				lon=None
#				loc_id=None
#				nominatim=None
#				tmp=None
#				k=None
#				v=None
#				kk=None
#				vv=None
#				o=None
	finally:
		output_queue.put(("DEATH_PILL_STOP_NOW",""))

def collectoutput(output_queue,csv_in,csv_out,csv_err,csv_offset,report_every):
	signal.signal(signal.SIGINT, signal.SIG_IGN) # ignore interrupt signals -> handle ctrl-c in main process

	started=False
	starttime=datetime.now() # corrected in first loop

	count=0
	deathpillstoswallowbeforedieing=multiprocessing.cpu_count()+1

	try:
		offset_=open(csv_offset,"r")
		offset=int(offset_.readline())
		offset_.close()
	except:
		offset=0
	
	lastcount=offset
	lastlap=starttime

	total=float(check_output(["wc","-l",csv_in]).split(" ")[0]) # while we are wasting time here counting lines, the worker processes already nibble on the real thing

	try:
		out=open(csv_out,"a")
		if offset==0:
			out.write(";".join(order_of_columns)+"\n")
		err=open(csv_err,"w")
	except:
		print("Troubles opening output and/or error file at %s and %s, respectively" %(csv_out,csv_err))
		exit()
	while True:
		(nominatim,is_error)=output_queue.get()
		if not started:					# don't count the line counting and skipping overhead into processing speed (-> ETA)
			starttime=datetime.now()
			started=True
		if nominatim=='DEATH_PILL_STOP_NOW':
			deathpillstoswallowbeforedieing-=1
			if deathpillstoswallowbeforedieing <= 0:
				break
			else:
				continue
		if is_error:  
			err.write(nominatim)
		else:
			out.write(nominatim)

		count+=1
		if count%report_every==0:
			laptime=datetime.now()
			ratio=count/(total-offset)
			elapsedtime=(laptime-starttime)
			elapsedtime=elapsedtime.total_seconds()
			totaltime=elapsedtime/ratio
			timeahead=totaltime-elapsedtime
			finishedtime=starttime+timedelta(0,totaltime)
			persecond=count/(elapsedtime*1.0)
			print("Processed %d+%d(=%d)/%d (%0.2f%%, %0.2f/s cur., %0.2f/s overall) in %s | %s remaining (ETA %s)"%(offset,count,(count+offset),total,(count+offset)*100.0/total,(count-lastcount)/((laptime-lastlap).total_seconds()*1.0),persecond,str(timedelta(0,elapsedtime)),str(timedelta(0,timeahead)),str(finishedtime)),end='\r')
			lastcount=count
			lastlap=laptime
	
			#if count%(report_every*10)==0:
			#	out.flush()
			#	os.fsync(out.fileno())
			#	err.flush()
			#	os.fsync(err.fileno())

		#nominatim=None
		#is_error=None

	err.close()
	out.close()
	finishtime=datetime.now()
	elapsedtime=(finishtime-starttime)
	persecond=total/1.0*elapsedtime.total_seconds()
	print("\n\n=======================================================\n\nFinished processing :-)\n\n% 16d lines processed in\n% 16d seconds\n                 (that is %s\n                 and %.2f lines/s)\n\nWe started our adventure    %s, \nand crossed the finish line %s.\n\n=======================================================\n" \
		%(                                             count,                    elapsedtime.total_seconds(),               str(elapsedtime),        count/(1.0*elapsedtime.total_seconds()),str(starttime),                   str(finishtime)))
	


if __name__ == '__main__':
	main()
